package ysmg.co.kr.util;

/**
 * Created by user on 2017-03-13.
 */

public interface Constants {
    static public String REPLY_JOIN_GROUP = "join_group";
    static public String REPLY_BAN_GROUP = "ban_group";
    static public String REPLY_SEND_NOTICE = "send_notice";
    static public String REPLY_SEND_MESSAGE = "send_message";
    static public String REPLY_RECEIVE_NOTICE = "receive_notice";
    static public String REPLY_RECEIVE_MESSAGE = "receive_message";
    static public String REPLY_REFRESH_USERLIST = "refresh_userlist";
    static public String REPLY_DISPOSE_GROUP = "dispose_group";

    static public String REPLY_ID = "id";
    static public String REPLY_NICK = "nick";
    static public String REPLY_MESSAGE = "message";

    static public String URL_STREAMMING_DEVELOP = "rtmp://live.xn--oi2bm8jmwiba250a.com/livestock/develop";
    static public String URL_REPLY_DEVELOP = "http://xn--o39aj1uk4gikhf2lnpcf4h.com/";
    static public String URL_LOGIN = "http://xn--o39aj1uk4gikhf2lnpcf4h.com/api/api.login.php";
    static public String URL_ANALYST = "http://xn--o39aj1uk4gikhf2lnpcf4h.com/api/api.anal_info.php";
    static public String URL_SIGN_UP = "http://xn--o39aj1uk4gikhf2lnpcf4h.com/api/api.sign_result.php";
    static public String URL_FCM_ID_REGISTER = "http://xn--o39aj1uk4gikhf2lnpcf4h.com/api/fcm_register.php";
    static public String URL_PUSH = "http://xn--o39aj1uk4gikhf2lnpcf4h.com/api/push_bundle.php";
    static public String URL_Logout = "http://xn--o39aj1uk4gikhf2lnpcf4h.com/api/api.logout.php";

    static public boolean REPLY_SOCKET_CONNECT = true;
    static public boolean REPLY_SOCKET_DISCONNECT = false;

    static public int DEFAULT_VIEWPAGER_INTERVAL = 5000;
    static public String CALL_CENTER_NUMBER = "tel:1670-6278";

    static final public String POST = "POST";
    static final public String GET = "GET";

    static final public String UTF_8 = "UTF-8";
    static final public String EUC_KR = "EUC-KR";

    static final public String MODE_IDCHECK = "idcheck";
    static final public String MODE_NICKCHECK = "nickcheck";
    static final public String MODE_USERINSERT = "userinsert";
    static final public String MODE_PHONECHECK = "phonecheck";
    static final public String MODE_READ_PUSH = "read_push";
    static final public String MODE_PULL_PUSH = "pull_push";
    static final public String MODE_UNKNOWN = "unknown";
    static final public String ID = "id="; //id=admin_E
    static final public String PW = "pw=";//pw=3671
    static final public String MODE = "mode=";
    static final public String PHONE_NUM = "phone_num=";
    static final public String NAME = "name=";
    static final public String IP = "ip=";
    static final public String NICK = "nick=";
    static final public String FCM_ID = "fcm_id=";
    static final public String DEV_ID = "dev_id=";
    static final public String APP_VERSION = "app_version=";
    static final public String DEVICE_OS = "device_os=";
    static final public String DEVICE_INFO = "device_info=";
    static final public String PUSH_ID = "push_id=";
    static final public String PAGE = "page=";
    static final public String AMPERSAND = "&";

    static final public int PERMISSIONS_REQUEST_CODE = 245;
    static final public int PUSH_PAGE_START_INDEX = 1;

    static final public String FCM_LOGINOTHERDEV = "loginotherdev";
    static final public String INTENT_GETPUSH = "intent_getpush";


}
