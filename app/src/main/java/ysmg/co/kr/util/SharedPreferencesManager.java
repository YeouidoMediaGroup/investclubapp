package ysmg.co.kr.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.Set;

import ysmg.co.kr.vo.Notification;
import ysmg.co.kr.vo.analyst.AnalystAll;
import ysmg.co.kr.vo.analyst.AnalystBroadcast;

/**
 * Created by user on 2017-03-08.
 * This class created for login Session control.
 *
 */

public class SharedPreferencesManager {
    static final private String TAG = SharedPreferencesManager.class.getSimpleName();
    static final private String FILENAME = "InvestClubApp";

    static final public String DEFAULT_STRING = null;
    static final public Set<String> DEFAULT_SET_STRING = null;
    static final public int DEFAULT_INT = 0;
    static final public boolean DEFAULT_BOOLEAN = false;
    /**
     * input value login_success boolean true:success false:fail
     */
    static final public String KEY_SUCCESS = "success";
    /***
     * input value login_id String
     */
    static final public String KEY_ID = "id";
    /**
     * input value login_pw String
     */
    static final public String KEY_PW = "pw";
    /**
     * input value login_message String
     */
    static final public String KEY_MESSAGE = "message";
    /**
     * input value login_analyst Set<String>
     */
    static final public String KEY_ANALYST = "analyst";
    /**
     * input value login_nickname String
     */
    static final public String KEY_NICK = "nick";
    /**
     * input value login_level int
     */
    static final public String KEY_LEVEL = "level";
    /**
     *  input value boolean, true : already getting first notice bunch, false : not yet, getting first notice bunch
     */
    static final public String KEY_IS_REPLY_FIRST_NOTICE_BUNCH = "is_first_notice_bunch";

    static final public String KEY_ANALYST_ALL = "analyst_all";
    static final public String KEY_ANALYST_BROADCAST = "analyst_broadcast";
    static final public String KEY_NOTIFICATION = "key_notification";

    static private SharedPreferencesManager instance;
    private Context context;
    private SharedPreferences pref;

    private ArrayList<AnalystAll> analystAlls;
    private ArrayList<AnalystBroadcast> analystBroadcasts;
    private ArrayList<Notification> notifications;


    private SharedPreferencesManager(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(FILENAME, Context.MODE_PRIVATE);

    }

    public static SharedPreferencesManager getInstance(){
        if(instance == null){
            Log.d(TAG, "SharedPreferencesManager.getInstance() is null");
            return null;
        }
        return instance;
    }

    public static SharedPreferencesManager createInstance(Context context){
        if(instance == null)
            instance = new SharedPreferencesManager(context);

        Log.d(TAG, "SharedPreferencesManager createInstance : " + instance);
        return instance;
    }

    public String getStringValue(String key) {
        return pref.getString(key, DEFAULT_STRING);
    }

    public void setStringValue(String key, String value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public Set<String> getStringSetValue(String key) {
        return pref.getStringSet(key, DEFAULT_SET_STRING);
    }

    public void setStringSetValue(String key, Set<String> value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putStringSet(key, value);
        editor.commit();
    }

    public int getIntValue(String key) {
        return pref.getInt(key, DEFAULT_INT);
    }

    public void setIntValue(String key, int value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public boolean getBooleanValue(String key) {
        return pref.getBoolean(key, DEFAULT_BOOLEAN);
    }

    public void setBooleanValue(String key, boolean value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public ArrayList getListValue(String key) {
        if(key.equals(KEY_ANALYST_ALL)) {
            return analystAlls;

        } else if (key.equals(KEY_ANALYST_BROADCAST)) {
            return analystBroadcasts;

        } else if (key.equals(KEY_NOTIFICATION)) {
            return notifications;

        }

        return null;
    }

    public void setListValue(String key, ArrayList list) {
            if(key.equals(KEY_ANALYST_ALL)) {
                analystAlls = list;

            } else if (key.equals(KEY_ANALYST_BROADCAST)) {
                analystBroadcasts = list;

            }else if (key.equals(KEY_NOTIFICATION)) {
                notifications = list;

            }
    }
}
