package ysmg.co.kr.util;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * Created by user on 2017-04-11.
 */

public class MySettingInfomation {
    static final private String TAG = MySettingInfomation.class.getSimpleName();
    static private MySettingInfomation instance;

    private Context context;

    private MySettingInfomation(Context context) {
        this.context = context;
    }

    public static MySettingInfomation getInstance(){
        if(instance == null){
            Log.d(TAG, "MySettingInfomation.getInstance() is null");
            return null;
        }
        return instance;
    }

    public static MySettingInfomation createInstance(Context context){
        if(instance == null)
            instance = new MySettingInfomation(context);

        Log.d(TAG, "MySettingInfomation createInstance : " + instance);
        return instance;
    }

    public String getMyPhoneNumber() {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        String number = telephonyManager.getLine1Number();
        if(number != null)
        {
            if(number.startsWith("+82"))
            {
                number = number.replace("+82", "0");
            }
        }else{
            number = "1234";
        }
        return number;
    }

    public String getIMEI() {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String IMEI= telephonyManager.getDeviceId();

        return IMEI;
    }
}
