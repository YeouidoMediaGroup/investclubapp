package ysmg.co.kr.util;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

public class LCDManager {
	static final private String TAG = LCDManager.class.getSimpleName();
    private Context context;
	private Window window;
	private static PowerManager.WakeLock wakeLock;
	static private LCDManager instance;

	public LCDManager(Context context) {
		this.context = context;
	}

	public static LCDManager getInstance(){
		if(instance == null){
			Log.d(TAG, "SharedPreferencesManager.getInstance() is null");
			return null;
		}
		return instance;
	}

	public static LCDManager createInstance(Context context){
		if(instance == null)
			instance = new LCDManager(context);

		Log.d(TAG, "LCDManager createInstance : " + instance);
		return instance;
	}

	public Window getWindow() {
		return window;
	}
	
	public LCDManager setWindow(Window window) {
		this.window = window;
		return this;
	}
	
	public void setLCDOn() {
		if(window != null) {
			PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
			wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "LCDManager");
			wakeLock.acquire();
			window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
			wakeLock.release();
		}
		Log.d(TAG, "setLCDOn() window : " + window + "...wakeLock : " + wakeLock);

	}
	public void setLCDAlwaysOn() {
		if(window != null) {
			if(wakeLock != null) return;
			PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
			wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP |                
	                PowerManager.ON_AFTER_RELEASE, "LCDManager");
			wakeLock.acquire();
			window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		}
		Log.d(TAG, "setLCDAlwaysOn() window : " + window + "...wakeLock : " + wakeLock);


	}
	public void setLCDOff() {
		if(wakeLock != null){
			wakeLock.release();
			wakeLock = null;
		}
		Log.d(TAG, "setLCDOff() wakeLock : " + wakeLock);
	}
	public void setLCDOnNoWindow() {
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "LCDManager");
		wakeLock.acquire();
		Log.d(TAG, "setLCDOnNoWindow() window : " + window + "...wakeLock : " + wakeLock);

	}
}