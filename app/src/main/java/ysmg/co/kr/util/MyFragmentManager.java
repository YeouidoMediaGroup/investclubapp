package ysmg.co.kr.util;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import ysmg.co.kr.R;


/**
 * Created by user on 2017-03-20.
 */

public class MyFragmentManager {
    static final private String TAG = MyFragmentManager.class.getSimpleName();
    static AppCompatActivity activity;
    static private MyFragmentManager instance;

    public MyFragmentManager(AppCompatActivity activity) {
        this.activity = activity;

    }

    public static MyFragmentManager getInstance(){
        if(instance == null){
            Log.d(TAG, "MyFragmentManager.getInstance() is null");
            return null;
        }
        return instance;
    }

    public static MyFragmentManager createInstance(AppCompatActivity activity){
        if(instance == null)
            instance = new MyFragmentManager(activity);

        Log.d(TAG, "MyFragmentManager createInstance : " + instance);
        return instance;
    }

    public static void replaceFragment(int R_id, Activity activity, Fragment fragment) {
        android.support.v4.app.FragmentManager fragmentManager = ((AppCompatActivity)activity).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R_id, fragment);
        try {
            fragmentTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrash.report(e);
//            fragmentTransaction.commitAllowingStateLoss();
        }

    }

    public void addFragment(Fragment fragment) {
        android.support.v4.app.FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container_main, fragment);
        fragmentTransaction.commitAllowingStateLoss();

    }

    public void removeFragment(Fragment fragment) {
        android.support.v4.app.FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }
}
