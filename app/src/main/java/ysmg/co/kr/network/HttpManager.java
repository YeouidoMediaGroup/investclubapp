package ysmg.co.kr.network;

import android.os.AsyncTask;

import com.google.firebase.crash.FirebaseCrash;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import ysmg.co.kr.util.Constants;

public class HttpManager {
	static private String TAG = HttpManager.class.getSimpleName();
	/**
	 * params[0]:GET,POST params[1]:url params[2]:body
	 * @return JsonString(LoginInfomation)
	 */
	static public class GETPOSTRequestThread extends AsyncTask<String, Void, String> {
		/**
		 * @param params params[0]:GET,POST params[1]:url params[2]:body
		 * @return JsonString(LoginInfomation)
		 */
		@Override
		protected String doInBackground(String... params) {
			String getPost = params[0];
			String request_url = params[1];
			String body = params[2];

			StringBuffer stringBuffer = new StringBuffer();
			try {
				URL url = new URL(request_url);
				HttpURLConnection conn = (HttpURLConnection)url.openConnection();

				conn.setRequestMethod(getPost);
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

				OutputStream outputStream = conn.getOutputStream();
				outputStream.write(body.getBytes(Constants.UTF_8));

				outputStream.flush();
				outputStream.close();

				String buffer;
				BufferedReader br = new BufferedReader( new InputStreamReader(conn.getInputStream(), Constants.UTF_8 ), conn.getContentLength());
				while((buffer = br.readLine()) != null) {
					stringBuffer.append(buffer);
				}

				br.close();
				conn.disconnect();
			}catch (IOException e){
				e.printStackTrace();
				FirebaseCrash.report(e);
			}

			String result = stringBuffer.toString();
			return result;
		}

		@Override
		protected void onPostExecute(String o) {
			super.onPostExecute(o);
		}

	}
}