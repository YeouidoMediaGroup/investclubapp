package ysmg.co.kr.network;

import ysmg.co.kr.vo.reply.ReplyBanGroup;
import ysmg.co.kr.vo.reply.ReplyDisposeGroup;
import ysmg.co.kr.vo.reply.ReplyJoinGroup;
import ysmg.co.kr.vo.reply.ReplyReceiveMessage;
import ysmg.co.kr.vo.reply.ReplyReceiveNotice;
import ysmg.co.kr.vo.reply.ReplyRefreshUserlist;

/**
 * Created by user on 2017-03-13.
 */

public interface SocketIOListener {

    public void socketConnectListener(boolean socketConnection);

    public void joingroupListener(ReplyJoinGroup replyJoinGroup);

    public void banGroupListener(ReplyBanGroup replyBanGroup);

    public void disposeGroupListener(ReplyDisposeGroup replyDisposeGroup);

    public void receiveMessageListener(ReplyReceiveMessage replyReceiveMessage);

    public void receiveNoticeListener(ReplyReceiveNotice replyReceiveNotice);

    public void refreshUserlistListener(ReplyRefreshUserlist replyRefreshUserlist);

}
