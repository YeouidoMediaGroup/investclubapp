package ysmg.co.kr.network;

import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import ysmg.co.kr.util.Constants;
import ysmg.co.kr.util.SharedPreferencesManager;
import ysmg.co.kr.vo.reply.ReplyBanGroup;
import ysmg.co.kr.vo.reply.ReplyDisposeGroup;
import ysmg.co.kr.vo.reply.ReplyJoinGroup;
import ysmg.co.kr.vo.reply.ReplyReceiveMessage;
import ysmg.co.kr.vo.reply.ReplyReceiveNotice;
import ysmg.co.kr.vo.reply.ReplyRefreshUserlist;

/**
 * Created by user on 2017-03-13.
 */

public class SocketIOManager {
    static final private String TAG = SocketIOManager.class.getSimpleName();
    static private SocketIOManager instance;
    static private io.socket.client.Socket mSocket;
    static private SocketIOListener socketIOCallback;
    static private Gson gson;

    private SocketIOManager() {

    }

    public static SocketIOManager getInstance(String url) {
        if (instance == null) {
            return null;
        }
        if (mSocket != null && mSocket.connected()) {
            Log.d(TAG, "Socket initialization : ");
            mSocket.disconnect();
            mSocket = null;
        }


        URI uri = URI.create(url);
        gson = new GsonBuilder().create();
        Log.d(TAG, "uri.getPath() : " + uri.getPath() + "...uri.getHost() : " + uri.getHost() + "...uri.toString() : " + uri.toString());
        mSocket = IO.socket(uri);
        SharedPreferencesManager.getInstance().setBooleanValue(SharedPreferencesManager.KEY_IS_REPLY_FIRST_NOTICE_BUNCH, false);
        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(TAG, "Socket.EVENT_CONNECT : ");
                socketIOCallback.socketConnectListener(Constants.REPLY_SOCKET_CONNECT);
                joinGroup();
                onBanGroup();
                onDisposeGroup();
                onReceiveMessage();
                onReceiveNotice();
                onRefreshUserlist();

            }

        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(TAG, "Socket.EVENT_DISCONNECT : ");
                socketIOCallback.socketConnectListener(Constants.REPLY_SOCKET_DISCONNECT);
            }
        });
        mSocket = mSocket.connect();
        return instance;
    }

    public static SocketIOManager createInstance() {
        if (instance == null)
            instance = new SocketIOManager();

        return instance;
    }

    private static void joinGroup() {
        JSONObject joingroupParams = new JSONObject();
        try {
            joingroupParams.put("id", /*SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_GROUP_ID)*/ "testgroup");
            joingroupParams.put("nick", SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_NICK));
        } catch (JSONException e) {
            e.printStackTrace();
            FirebaseCrash.report(e);
        }

        Log.d(TAG, "joinGroup() joingroupParams : " + joingroupParams);

        mSocket.emit(Constants.REPLY_JOIN_GROUP, joingroupParams);

        mSocket.on(Constants.REPLY_JOIN_GROUP, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                ReplyJoinGroup replyJoinGroup = (ReplyJoinGroup) gson.fromJson(args[0].toString(), ReplyJoinGroup.class);
                socketIOCallback.joingroupListener(replyJoinGroup);
//                Log.d(TAG, "joinGroup() replyJoinGroup : " + replyJoinGroup.toString());
            }
        });
    }

    private static void onBanGroup() {
        mSocket.on(Constants.REPLY_BAN_GROUP, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                ReplyBanGroup replyBanGroup = (ReplyBanGroup) gson.fromJson(args[0].toString(), ReplyBanGroup.class);
                socketIOCallback.banGroupListener(replyBanGroup);
                Log.d(TAG, "joinGroup() REPLY_BAN_GROUP : " + Constants.REPLY_BAN_GROUP);
            }
        });
    }

    private static void onDisposeGroup() {
        mSocket.on(Constants.REPLY_DISPOSE_GROUP, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                ReplyDisposeGroup replyDisposeGroup = (ReplyDisposeGroup) gson.fromJson(args[0].toString(), ReplyDisposeGroup.class);
                socketIOCallback.disposeGroupListener(replyDisposeGroup);
                Log.d(TAG, "joinGroup() REPLY_DISPOSE_GROUP : " + Constants.REPLY_DISPOSE_GROUP);
            }
        });
    }

    private static void onReceiveMessage() {
        mSocket.on(Constants.REPLY_RECEIVE_MESSAGE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONArray jsonArray = (JSONArray) args[0];
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        ReplyReceiveMessage replyReceiveMessage = gson.fromJson(jsonArray.getString(i), ReplyReceiveMessage.class);
                        socketIOCallback.receiveMessageListener(replyReceiveMessage);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        FirebaseCrash.report(e);
                    }
                }
            }
        });
    }

    private static void onReceiveNotice() {
        mSocket.on(Constants.REPLY_RECEIVE_NOTICE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONArray jsonArray = (JSONArray) args[0];

                //채팅서버 연결이 알수없는 이유로 끊어졌다 재접속이 될 경우 초기 NOTICE BUNCH값이 다시 전송되는 경우 있음.
                if (jsonArray != null && jsonArray.length() > 1 && !SharedPreferencesManager.getInstance().getBooleanValue(SharedPreferencesManager.KEY_IS_REPLY_FIRST_NOTICE_BUNCH)) {
                    SharedPreferencesManager.getInstance().setBooleanValue(SharedPreferencesManager.KEY_IS_REPLY_FIRST_NOTICE_BUNCH, true);
                    Log.d(TAG, "joinGroup() REPLY_RECEIVE_NOTICE : not yet, had not getting first notice bunch :");

                } else if (jsonArray != null && jsonArray.length() > 1 && SharedPreferencesManager.getInstance().getBooleanValue(SharedPreferencesManager.KEY_IS_REPLY_FIRST_NOTICE_BUNCH)) {
                    Log.d(TAG, "joinGroup() REPLY_RECEIVE_NOTICE : already, had getting first notice bunch :");
                    return;
                }

                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        ReplyReceiveNotice replyReceiveNotice = gson.fromJson(jsonArray.getString(i), ReplyReceiveNotice.class);
                        socketIOCallback.receiveNoticeListener(replyReceiveNotice);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        FirebaseCrash.report(e);
                    }
                }
            }
        });
    }

    private static void onRefreshUserlist() {
        mSocket.on(Constants.REPLY_REFRESH_USERLIST, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                ReplyRefreshUserlist replyRefreshUserlist = (ReplyRefreshUserlist) gson.fromJson(args[0].toString(), ReplyRefreshUserlist.class);
                socketIOCallback.refreshUserlistListener(replyRefreshUserlist);
//                Log.d(TAG, "joinGroup() REPLY_REFRESH_USERLIST : " + Constants.REPLY_REFRESH_USERLIST);
            }
        });
    }

    public static void emitBanGroup(String nick) {
        JSONObject banGroupParams = new JSONObject();
        try {
            banGroupParams.put(Constants.REPLY_NICK, nick);
        } catch (JSONException e) {
            e.printStackTrace();
            FirebaseCrash.report(e);
        }
        mSocket.emit(Constants.REPLY_BAN_GROUP, banGroupParams);

    }

    @Deprecated
    public void emitSendNotice(String message) {
        JSONObject emitSendNoticeParams = new JSONObject();
        try {
            emitSendNoticeParams.put(Constants.REPLY_MESSAGE, message);
        } catch (JSONException e) {
            e.printStackTrace();
            FirebaseCrash.report(e);
        }
        Log.d(TAG, "emitSendNotice() emitSendNoticeParams : " + emitSendNoticeParams);
        Log.d(TAG, "mSocket.connected() mSocket.connected() : " + mSocket.connected());

        mSocket.emit(Constants.REPLY_SEND_NOTICE, emitSendNoticeParams);
    }

    public void emitSendMessage(String message) {
        JSONObject emitSendMessageParams = new JSONObject();
        try {
            emitSendMessageParams.put(Constants.REPLY_MESSAGE, message);
        } catch (JSONException e) {
            e.printStackTrace();
            FirebaseCrash.report(e);
        }
        Log.d(TAG, "emitSendMessage() emitSendMessageParams : " + emitSendMessageParams);
        Log.d(TAG, "mSocket.connected() mSocket.connected() : " + mSocket.connected());

        mSocket.emit(Constants.REPLY_SEND_MESSAGE, emitSendMessageParams);
    }

    public boolean isconnected() {
        if (mSocket != null)
            return mSocket.connected();
        return false;
    }

    public void disconnect() {
        if (mSocket != null)
            mSocket.disconnect();
    }


    public void setCallback(SocketIOListener socketIOCallback) {
        this.socketIOCallback = socketIOCallback;
    }
}
