package ysmg.co.kr.view.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import ysmg.co.kr.R;

/**
 * Created by user on 2017-03-17.
 */

public class BannerAdapter extends PagerAdapter{
    static private String TAG = BannerAdapter.class.getSimpleName();
    private LayoutInflater inflater;
    private int[] mResources = {
            R.drawable.visual_00,
            R.drawable.visual_01,
            R.drawable.visual_02,
    };
    public BannerAdapter(Context context){
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((FrameLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = inflater.inflate(R.layout.viewpager_container, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.img_viewpager_childimage);

        imageView.setImageResource(mResources[position]);

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }
}
