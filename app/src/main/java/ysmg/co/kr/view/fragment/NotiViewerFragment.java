package ysmg.co.kr.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

import ysmg.co.kr.R;
import ysmg.co.kr.network.HttpManager;
import ysmg.co.kr.util.Constants;
import ysmg.co.kr.util.MyFragmentManager;
import ysmg.co.kr.util.SharedPreferencesManager;
import ysmg.co.kr.view.adapter.NotiListAdapter;
import ysmg.co.kr.vo.Notification;
import ysmg.co.kr.vo.NotificationBundle;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {link NotiViewerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NotiViewerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NotiViewerFragment extends Fragment {
    static private String TAG = NotiViewerFragment.class.getSimpleName();
    private ListView listView_noti;
    private NotiListAdapter notiListAdapter;
    private View view;
    private ArrayList<Notification> arrayList_noti = new ArrayList<>();;
    private int page, page_max;
    private Gson gson;
    public NotiViewerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment NotiViewerFragment.
     */
    public static NotiViewerFragment newInstance() {
        NotiViewerFragment fragment = new NotiViewerFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_noti_viewer, container, false);
        try {
            initializationPushViewerFragment(view);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onResume() {
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(onKeyListener);
        super.onResume();
    }

    private void initializationPushViewerFragment(View view) throws ExecutionException, InterruptedException {
        listView_noti =  (ListView) view.findViewById(R.id.listView_noti);
        gson = new Gson();
        page = Constants.PUSH_PAGE_START_INDEX;

        HttpManager.GETPOSTRequestThread task = new HttpManager.GETPOSTRequestThread();
        String pushresult = task.execute(Constants.POST, Constants.URL_PUSH, getPushbody(null)).get();
        Log.d(TAG, "pushresult " + pushresult);

        NotificationBundle notificationBundle = (NotificationBundle) gson.fromJson(pushresult, NotificationBundle.class);
        page_max = ((notificationBundle.getLength() == 0) ? 1 : 0);
        if(notificationBundle.getLength() != 0) {
            arrayList_noti = new ArrayList<Notification>(Arrays.asList(notificationBundle.getPush()));
            page_max = page_max + ((notificationBundle.getLength() % 8 != 0) ? 1 : 0);
            page_max = page_max + notificationBundle.getLength() / 8;

        }

        Log.d(TAG, "notificationBundle.getLength() : " + notificationBundle.getLength() + "...page_max : " + page_max);

        notiListAdapter = new NotiListAdapter(getActivity(), arrayList_noti);
        listView_noti.setAdapter(notiListAdapter);
        listView_noti.setOnItemClickListener(onItemClickListener);
    }

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d(TAG, "onItemClick position : " + position);
            //더보기를 눌렀을때 새로운 push메시지를 받는다(20개)
            if(position == arrayList_noti.size()) {
                Log.d(TAG, "page : " + page + "page_max : " + page_max);
                if(page == page_max) {
                    Toast.makeText(getContext(), "더이상 불러올 데이터가 없습니다.", Toast.LENGTH_LONG).show();
                    return;
                }
                page++;

                HttpManager.GETPOSTRequestThread task = new HttpManager.GETPOSTRequestThread();
                try {
                    String pushresult = task.execute(Constants.POST, Constants.URL_PUSH, getPushbody(null)).get();
                    NotificationBundle notificationBundle = (NotificationBundle) gson.fromJson(pushresult, NotificationBundle.class);
                    Log.d(TAG, "notificationBundle : " + notificationBundle.toString());
                    if(notificationBundle.getLength() != 0)
                        arrayList_noti.addAll(new ArrayList<Notification>(Arrays.asList(notificationBundle.getPush())));

                } catch (InterruptedException e) {
                    e.printStackTrace();

                } catch (ExecutionException e) {
                    e.printStackTrace();

                }
            }

            notiListAdapter.notifyDataSetChanged();
        }
    };

    private View.OnKeyListener onKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
                if(drawer==null)
                    return false;

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);

                } else {
                    MyFragmentManager.getInstance().replaceFragment(R.id.fragment_container_main, getActivity(), SelectBroadcastFragment.newInstance());

                }
                return true;
            }
            return false;
        }
    };

    private String getPushbody(String push_id) {
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer
                .append(Constants.ID).append(SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_ID)).append(Constants.AMPERSAND)
                .append(Constants.MODE).append(Constants.MODE_PULL_PUSH).append(Constants.AMPERSAND)
                .append(Constants.PUSH_ID).append(push_id).append(Constants.AMPERSAND)
                .append(Constants.PAGE).append(page);

        return stringBuffer.toString();
    }
}
