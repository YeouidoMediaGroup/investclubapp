package ysmg.co.kr.view.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import ysmg.co.kr.R;
import ysmg.co.kr.util.MyFragmentManager;
import ysmg.co.kr.view.fragment.TermsFragment;

public class SignUpActivity extends AppCompatActivity {
    static private String TAG = SignUpActivity.class.getSimpleName();
    private ImageButton button_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        this.initializationSignUpActivity();
    }

    private void initializationSignUpActivity() {
//        MyFragmentManager.replaceFragment(R.id.fragment_container_sign_up, this, NotiViewerFragment.newInstance());
        MyFragmentManager.replaceFragment(R.id.fragment_container_sign_up, this, TermsFragment.newInstance());
        button_back = (ImageButton) findViewById(R.id.button_back);
        button_back.setOnClickListener(onClickListener);

        Log.d(TAG, "MainActivity initializationSignUpActivity()");
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
                if(v.getId() == R.id.button_back){
                    showDialog(getString(R.string.sign_up), getString(R.string.sing_up_end_message), getString(R.string.ok), getString(R.string.cancel));
                }
        }
    };

    @Override
    public void onBackPressed() {
        showDialog(getString(R.string.sign_up), getString(R.string.sing_up_end_message), getString(R.string.ok), getString(R.string.cancel));
    }

    private void showDialog(String title, String message, String positiveButtonMessage, String negativeButtonMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(positiveButtonMessage, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(negativeButtonMessage, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
