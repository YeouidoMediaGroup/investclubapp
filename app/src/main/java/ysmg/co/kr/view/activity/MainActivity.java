package ysmg.co.kr.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import ysmg.co.kr.R;
import ysmg.co.kr.network.HttpManager;
import ysmg.co.kr.util.Constants;
import ysmg.co.kr.util.MyFragmentManager;
import ysmg.co.kr.util.SharedPreferencesManager;
import ysmg.co.kr.view.adapter.NavAnalystListAdapter;
import ysmg.co.kr.view.fragment.NotiViewerFragment;
import ysmg.co.kr.view.fragment.SelectBroadcastFragment;
import ysmg.co.kr.vo.LoginInfomation;
import ysmg.co.kr.vo.analyst.AnalystAll;
import ysmg.co.kr.vo.analyst.AnalystBroadcast;


public class MainActivity extends AppCompatActivity {
    static private String TAG = MainActivity.class.getSimpleName();
    private View navigationHeader, convertView;
    private NavigationView navigationView;
    private StringBuffer stringBuffer;
    private ImageButton imageButton_logout, imageButton_modify, imageButton_noti;
    private TextView textview_login_info;
    private ListView listView_nav_analyst;
    private NavAnalystListAdapter navAnalystListAdapter;
    private Timer tokenTimer;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(""); //toolbar 타이틀을 지우고 xml에서 새로운 타이틀을 style로 정의한다.
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationHeader = navigationView.inflateHeaderView(R.layout.nav_header_main);
        this.tokenRefresh();
        this.initializationMainActivity();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Log.d(TAG, "MainActivity onBackPressed()  ");
//            super.onBackPressed();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "MainActivity onDestroy()  ");
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.imageButton_logout) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(Constants.ID).append(SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_ID));

                HttpManager.GETPOSTRequestThread task = new HttpManager.GETPOSTRequestThread();
                try {
                    String result = task.execute(Constants.POST, Constants.URL_Logout, stringBuffer.toString()).get();
                    Log.d(TAG, "logout result : " + result);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                LoginInfomation.updateLoginInfomation(null);
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            } else if (id == R.id.imageButton_modify) {
            } else if (id == R.id.imageButton_noti) {
                MyFragmentManager.replaceFragment(R.id.fragment_container_main, MainActivity.this, NotiViewerFragment.newInstance());
            }
        }
    };

    /**
     * This method developed for that Initialize this application.\n
     * Representatively, Android Widget, Database(SharedPreference), Listener are Initialized
     */

    private void initializationMainActivity() {
        if((getIntent().getStringExtra(Constants.INTENT_GETPUSH) != null) && (getIntent().getStringExtra(Constants.INTENT_GETPUSH)).equals(Constants.INTENT_GETPUSH)){
            MyFragmentManager.replaceFragment(R.id.fragment_container_main, this, NotiViewerFragment.newInstance());

        } else {
            MyFragmentManager.replaceFragment(R.id.fragment_container_main, this, SelectBroadcastFragment.newInstance());

        }


        imageButton_logout = (ImageButton) navigationHeader.findViewById(R.id.imageButton_logout);
        imageButton_modify = (ImageButton) navigationHeader.findViewById(R.id.imageButton_modify);

        textview_login_info = (TextView) navigationHeader.findViewById(R.id.textview_login_info);

        listView_nav_analyst = (ListView) navigationView.findViewById(R.id.listView_nav_analyst);
        imageButton_noti = (ImageButton)  toolbar.findViewById(R.id.imageButton_noti);

        imageButton_logout.setOnClickListener(onClickListener);
        imageButton_modify.setOnClickListener(onClickListener);
        imageButton_noti.setOnClickListener(onClickListener);

        navAnalystListAdapter = new NavAnalystListAdapter(getApplicationContext(), SharedPreferencesManager.getInstance().getListValue(SharedPreferencesManager.KEY_ANALYST_BROADCAST));
        listView_nav_analyst.setAdapter(navAnalystListAdapter);
        listView_nav_analyst.setOnItemClickListener(onItemClickListener);

        stringBuffer = new StringBuffer();
        stringBuffer
                .append(SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_NICK)).append("님 ")
                .append("(").append(SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_ID)).append(")");

        textview_login_info.setText(stringBuffer.toString());

        //TODO:testcode
        ArrayList<AnalystAll> analystAlls = SharedPreferencesManager.getInstance().getListValue(SharedPreferencesManager.KEY_ANALYST_ALL);
        ArrayList<AnalystBroadcast> analystBroadcasts = SharedPreferencesManager.getInstance().getListValue(SharedPreferencesManager.KEY_ANALYST_BROADCAST);

        Log.d(TAG, "analystAlls.size() : " + analystAlls.size() + "...analystBroadcasts.size() : " + analystBroadcasts.size());
        Log.d(TAG, "MainActivity initializationMainActivity()");
    }

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if(convertView != null) {
                convertView.setBackgroundResource(R.drawable.nav_background);
            }

            Toast.makeText(getApplicationContext(), getString(R.string.ready_for_broadcast), Toast.LENGTH_SHORT).show();
            view.setBackgroundResource(R.drawable.nav_background_on);

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);

            convertView = view;
        }
    };

    private void tokenRefresh(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                } catch (IOException e) {
                    FirebaseCrash.report(e);
                }
            }
        }).start();
        //
        if(tokenTimer != null){
            tokenTimer.cancel();
            tokenTimer.purge();
            tokenTimer= null;
        }
        tokenTimer = new Timer();
        tokenTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                String token = FirebaseInstanceId.getInstance().getToken();
                Log.d(TAG, "MainActivity tokenRefresh() : " + token);
            }
        }, 5000);
    }
    public Toolbar getToolbar(){
        return toolbar;
    }

    public NavigationView getNavigationView(){
        return navigationView;
    }

    public View getConvertView(){
        return convertView;
    }
}
