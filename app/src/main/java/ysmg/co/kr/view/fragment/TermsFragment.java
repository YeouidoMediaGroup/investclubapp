package ysmg.co.kr.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.Toast;

import ysmg.co.kr.R;
import ysmg.co.kr.util.MyFragmentManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {TermsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TermsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TermsFragment extends Fragment {
    private View view;
    private CheckBox checkBox_terms_01, checkBox_terms_02;
    private ImageButton button_sign_up;

    public TermsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TermsFragment.
     */
    public static TermsFragment newInstance() {
        TermsFragment fragment = new TermsFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_terms, container, false);
        this.initializationTermsFragment(view);

        return view;

    }

    private void initializationTermsFragment(View view) {
        checkBox_terms_01 =  (CheckBox) view.findViewById(R.id.checkBox_terms_01);
        checkBox_terms_02 =  (CheckBox) view.findViewById(R.id.checkBox_terms_02);
        button_sign_up =  (ImageButton) view.findViewById(R.id.button_sign_up);

        button_sign_up.setOnClickListener(onClickListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.button_sign_up) {
                if(checkBox_terms_01.isChecked() && checkBox_terms_02.isChecked()){
                    MyFragmentManager.getInstance().replaceFragment(R.id.fragment_container_sign_up, getActivity(), SignUpFragment.newInstance(checkBox_terms_01.isChecked(), checkBox_terms_02.isChecked()));

                } else {
                    Toast.makeText(getContext(), getString(R.string.unchecked_terms), Toast.LENGTH_LONG).show();

                }
            }
        }
    };
}
