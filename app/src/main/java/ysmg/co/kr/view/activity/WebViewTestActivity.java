package ysmg.co.kr.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ysmg.co.kr.R;

public class WebViewTestActivity extends AppCompatActivity {
    WebView webView;
    Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_test);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        activity = this;
    }

    @Override
    protected void onResume() {
        super.onResume();

//        goURL();
    }

    public void goURL(){
        String url = "http://boss.xn--o39aj1uk4gikhf2lnpcf4h.com/index_sb.php";
        Log.e("URL", "Opening URL with WebView :" + url);
        Log.e("TestWebView","reloadwebView");
        final long startTime = System.currentTimeMillis();
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setJavaScriptEnabled(true);
        // 하드웨어 가속
        // 캐쉬 끄기
        //webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

            }
        });

        webView.loadUrl(url);

    }
}
