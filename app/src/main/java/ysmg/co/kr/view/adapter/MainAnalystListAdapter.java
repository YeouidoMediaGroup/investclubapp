package ysmg.co.kr.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ysmg.co.kr.R;
import ysmg.co.kr.view.activity.MainActivity;
import ysmg.co.kr.vo.analyst.AnalystAll;

/**
 * Created by user on 2017-04-05.
 */

public class MainAnalystListAdapter extends BaseAdapter {
    static private String TAG = MainAnalystListAdapter.class.getSimpleName();

    private Activity activity;
    private ArrayList<AnalystAll> analystAlls;
    private ImageButton imageButton_enter_broadcast;
    private TextView textView_analyst_nick;
    private ImageView imageView_level;

    public MainAnalystListAdapter(Activity activity, ArrayList<AnalystAll> analystAlls) {
        this.activity = activity;
        this.analystAlls = analystAlls;
    }

    @Override
    public int getCount() {

        return analystAlls.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.listview_item_main_analyst, parent, false);
        imageButton_enter_broadcast = (ImageButton) view.findViewById(R.id.imageButton_enter_broadcast);
        textView_analyst_nick =  (TextView) view.findViewById(R.id.textView_analyst_nick);
        imageView_level =  (ImageView) view.findViewById(R.id.imageView_level);

        textView_analyst_nick.setText(analystAlls.get(position).getNick());
        imageButton_enter_broadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((MainActivity)activity).getConvertView() != null) {
                    ((MainActivity)activity).getConvertView().setBackgroundResource(R.drawable.nav_background);
                }

                Toast.makeText(activity, activity.getString(R.string.ready_for_broadcast), Toast.LENGTH_SHORT).show();

            }
        });

        return view;
    }

}
