package ysmg.co.kr.view.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import ysmg.co.kr.R;
import ysmg.co.kr.network.HttpManager;
import ysmg.co.kr.util.Constants;
import ysmg.co.kr.util.MyFragmentManager;
import ysmg.co.kr.util.MySettingInfomation;
import ysmg.co.kr.view.activity.LoginActivity;
import ysmg.co.kr.vo.SignUpResponse;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {SignUpFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SignUpFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignUpFragment extends Fragment {
    static final private String TAG = SignUpFragment.class.getSimpleName();
    private static final String ARG_TERMS_01 = "terms_01";
    private static final String ARG_TERMS_02 = "terms_02";

    private boolean terms_01;
    private boolean terms_02;

    private View view;
    private ImageButton button_sign_up_complete, button_sign_up_cancel, button_id_overlap, button_nick_overlap;
    private EditText editText_id, editText_nick, editText_pw, editText_pw_check, editText_name;
    private TextView textView_id_caution, textView_nick_caution;

    private boolean id_check; //id 중복 확인
    private boolean nick_check; // 닉네임 중복 확인
    private Gson gson;

    public SignUpFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param terms_01 Parameter 1.
     * @param terms_02 Parameter 2.
     * @return A new instance of fragment SignUpFragment.
     */
    public static SignUpFragment newInstance(boolean terms_01, boolean terms_02) {
        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_TERMS_01, terms_01);
        args.putBoolean(ARG_TERMS_02, terms_02);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            terms_01 = getArguments().getBoolean(ARG_TERMS_01);
            terms_02 = getArguments().getBoolean(ARG_TERMS_02);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        this.termsCheck();
        this.initializationSignUpFragment(view);

        try {
            this.startSignup(Constants.MODE_PHONECHECK);
        } catch (ExecutionException e) {
            FirebaseCrash.report(e);

        } catch (InterruptedException e) {
            FirebaseCrash.report(e);

        }

        return view;
    }

    private void termsCheck() {
        //만약 약관동의 진행하지않고 들어왔을경우를 대비한 코드 작성해야함(만약을위한..?)
        if (!terms_01 || !terms_02) {
            Toast.makeText(getContext(), getString(R.string.unchecked_terms), Toast.LENGTH_LONG).show();
            MyFragmentManager.getInstance().replaceFragment(R.id.fragment_container_sign_up, getActivity(), TermsFragment.newInstance());

        }
    }

    private void initializationSignUpFragment(View view) {
        id_check = false;
        nick_check = false;

        button_sign_up_complete = (ImageButton) view.findViewById(R.id.button_sign_up_complete);
        button_sign_up_cancel = (ImageButton) view.findViewById(R.id.button_sign_up_cancel);
        button_id_overlap = (ImageButton) view.findViewById(R.id.button_id_overlap);
        button_nick_overlap = (ImageButton) view.findViewById(R.id.button_nick_overlap);

        editText_id = (EditText) view.findViewById(R.id.editText_id);
        editText_nick = (EditText) view.findViewById(R.id.editText_nick);
        editText_pw = (EditText) view.findViewById(R.id.editText_pw);
        editText_pw_check = (EditText) view.findViewById(R.id.editText_pw_check);
        editText_name = (EditText) view.findViewById(R.id.editText_name);

        textView_id_caution = (TextView) view.findViewById(R.id.textView_id_caution);
        textView_nick_caution = (TextView) view.findViewById(R.id.textView_nick_caution);

        button_sign_up_complete.setOnClickListener(onClickListener);
        button_sign_up_cancel.setOnClickListener(onClickListener);
        button_id_overlap.setOnClickListener(onClickListener);
        button_nick_overlap.setOnClickListener(onClickListener);

        editText_id.addTextChangedListener(id_textWatcher);
        editText_nick.addTextChangedListener(nick_textWatcher);

        gson = new Gson();

    }

    TextWatcher id_textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (id_check) {
                id_check = false;
                textView_id_caution.setText(getString(R.string.sing_up_id_caution));
                textView_id_caution.setTextColor(ContextCompat.getColor(getContext(), R.color.colorcaution));
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    TextWatcher nick_textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (nick_check) {
                nick_check = false;
                textView_nick_caution.setText(getString(R.string.sing_up_nick_caution));
                textView_nick_caution.setTextColor(ContextCompat.getColor(getContext(), R.color.colorcaution));
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            String mode = Constants.MODE_UNKNOWN;
            switch (v.getId()) {
                case R.id.button_id_overlap:
                    mode = Constants.MODE_IDCHECK;
                    break;

                case R.id.button_nick_overlap:
                    mode = Constants.MODE_NICKCHECK;
                    break;

                case R.id.button_sign_up_complete:
                    mode = Constants.MODE_USERINSERT;
                    break;

                case R.id.button_sign_up_cancel:
                    showDialog(getString(R.string.sign_up), getString(R.string.sing_up_end_message), getString(R.string.ok), getString(R.string.cancel));
                    return;
            }

            if (getValidation(mode)) {
                try {
                    startSignup(mode);
                } catch (ExecutionException e) {
                    FirebaseCrash.report(e);

                } catch (InterruptedException e) {
                    FirebaseCrash.report(e);

                }
            }
        }
    };

    private void startSignup(String mode) throws ExecutionException, InterruptedException {
        String body = getSignUpRequestBody(mode);
        HttpManager.GETPOSTRequestThread task = new HttpManager.GETPOSTRequestThread();
        String result = task.execute(Constants.POST, Constants.URL_SIGN_UP, body).get();
        SignUpResponse signUpResponse = (SignUpResponse) gson.fromJson(result, SignUpResponse.class);

        switch (mode) {
            case Constants.MODE_IDCHECK:
                if (signUpResponse.isSuccess()) {
                    id_check = true;
                    textView_id_caution.setText(getString(R.string.sing_up_id_caution_ok));
                    textView_id_caution.setTextColor(ContextCompat.getColor(getContext(), R.color.colorcaution_ok));

                } else {
                    editText_id.setError(getString(R.string.sing_up_id_error_02));

                }
                break;

            case Constants.MODE_NICKCHECK:
                if (signUpResponse.isSuccess()) {
                    nick_check = true;
                    textView_nick_caution.setText(getString(R.string.sing_up_nick_caution_ok));
                    textView_nick_caution.setTextColor(ContextCompat.getColor(getContext(), R.color.colorcaution_ok));

                } else {
                    editText_nick.setError(getString(R.string.sing_up_nick_error_02));

                }
                break;

            case Constants.MODE_USERINSERT:
                if (signUpResponse.isSuccess()) {
                    showDialog(true, getString(R.string.sign_up), getString(R.string.sing_up_success_message), getString(R.string.ok));

                } else {
                    showDialog(false, getString(R.string.sign_up_fail), signUpResponse.getMessage(), getString(R.string.ok));

                }

                break;

            case Constants.MODE_PHONECHECK:
                if (signUpResponse.isSuccess()) {


                } else {
                    showDialog(true, getString(R.string.sign_up_fail), getString(R.string.sing_up_fail_overlap_id), getString(R.string.ok));

                }
                break;

            default:

                break;
        }

    }

    private boolean getValidation(String mode) {
        boolean validation = true;
        String id = editText_id.getText().toString();
        String pw = editText_pw.getText().toString();
        String pw_check = editText_pw_check.getText().toString();
        String name = editText_name.getText().toString();
        String nick = editText_nick.getText().toString();

        String id_regex = "^[a-zA-Z]{1}[a-zA-Z0-9_]{2,11}$";
        String nick_eng_regex = "^[a-zA-Z0-9]{4,16}$";
        String nick_kor_regex = "^[ㄱ-ㅎ가-힣0-9]{2,8}$";
        String pw_regex = "([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9]){8,20}$";

        switch (mode) {
            case Constants.MODE_IDCHECK:
                if (!Pattern.matches(id_regex, id)) {
                    editText_id.setError(getString(R.string.sing_up_id_error_00));
                    validation = false;

                }

                break;

            case Constants.MODE_NICKCHECK:
                if (!Pattern.matches(nick_eng_regex, nick) && !Pattern.matches(nick_kor_regex, nick)) {
                    editText_nick.setError(getString(R.string.sing_up_nick_error_00));
                    validation = false;
                }

                break;

            case Constants.MODE_USERINSERT:
                if (!id_check) {
                    editText_id.setError(getString(R.string.sing_up_id_error_01));
                    validation = false;
                }

                if (!nick_check) {
                    editText_nick.setError(getString(R.string.sing_up_nick_error_01));
                    validation = false;
                }

                if (name == null || name.length() < 1) {
                    editText_name.setError(getString(R.string.sing_up_name_error));
                }

                if (!Pattern.matches(pw_regex, pw)) {
                    editText_pw.setError(getString(R.string.sing_up_pw_error_00));
                    validation = false;

                } else {
                    if (!pw.equals(pw_check)) {
                        editText_pw_check.setText("");
                        editText_pw_check.setError(getString(R.string.sing_up_pw_error_01));
                        validation = false;

                    }

                }

                break;

            default:
                validation = false;
                try {
                    throw new Exception("SignUpFragment getValidation - unknown mode : " + mode);
                } catch (Exception e) {
                    FirebaseCrash.report(e);
                }

                break;

        }
        return validation;
    }

    private String getSignUpRequestBody(String mode) {
        StringBuffer stringBuffer = new StringBuffer();
        String phone_num = MySettingInfomation.getInstance().getMyPhoneNumber();
        String id = editText_id.getText().toString();
        String pw = editText_pw.getText().toString();
        String name = editText_name.getText().toString();
        String nick = editText_nick.getText().toString();
        String ip = getLocalIpAddress();

        stringBuffer
                .append(Constants.MODE).append(mode).append(Constants.AMPERSAND)
                .append(Constants.PHONE_NUM).append(phone_num).append(Constants.AMPERSAND)
                .append(Constants.ID).append(id).append(Constants.AMPERSAND)
                .append(Constants.PW).append(pw).append(Constants.AMPERSAND)
                .append(Constants.NAME).append(name).append(Constants.AMPERSAND)
                .append(Constants.IP).append(ip).append(Constants.AMPERSAND)
                .append(Constants.NICK).append(nick);

        Log.d(TAG, "getSignUpRequestBody jsonString : " + stringBuffer.toString());

        return stringBuffer.toString();
    }

    private void showDialog(String title, String message, String positiveButtonMessage, String negativeButtonMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(positiveButtonMessage, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                })

                .setNegativeButton(negativeButtonMessage, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDialog(final boolean isSuccess, String title, String message, String positiveButtonMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(positiveButtonMessage, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if(isSuccess) {
                            Intent intent = new Intent(getContext(), LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            dialog.cancel();
                        }
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i(TAG, "***** IP="+ ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException e) {
            FirebaseCrash.report(e);

        }
        return null;
    }
}
