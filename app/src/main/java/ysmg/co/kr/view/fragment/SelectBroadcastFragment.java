package ysmg.co.kr.view.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import ysmg.co.kr.R;
import ysmg.co.kr.util.Constants;
import ysmg.co.kr.util.SharedPreferencesManager;
import ysmg.co.kr.view.activity.MainActivity;
import ysmg.co.kr.view.adapter.BannerAdapter;
import ysmg.co.kr.view.adapter.MainAnalystListAdapter;
import ysmg.co.kr.view.widget.AutoScrollViewPager;

public class SelectBroadcastFragment extends Fragment {
    static private String TAG = SelectBroadcastFragment.class.getSimpleName();
    private View view;
    private AutoScrollViewPager autoscrollviewpager_select;
    private ImageView indicator_00, indicator_01, indicator_02, imageView_callcenter;
    private NavigationView navigationView;
    private Toolbar toolbar;
    private ListView listView_main_analyst;
    private MainAnalystListAdapter mainAnalystListAdapter;

    public SelectBroadcastFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SelectBroadcastFragment.
     */
    public static SelectBroadcastFragment newInstance() {
        SelectBroadcastFragment fragment = new SelectBroadcastFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_select_broadcast, container, false);
        this.initializationSelectBroadcastFragment(view);

        return view;
    }

    @Override
    public void onPause() {
        autoscrollviewpager_select.stopAutoScroll();
        super.onPause();
    }

    private void initializationSelectBroadcastFragment(View view) {

        autoscrollviewpager_select = (AutoScrollViewPager) view.findViewById(R.id.autoscrollviewpager_select);
        indicator_00 = (ImageView) view.findViewById(R.id.indicator_00);
        indicator_01 = (ImageView) view.findViewById(R.id.indicator_01);
        indicator_02 = (ImageView) view.findViewById(R.id.indicator_02);
        imageView_callcenter = (ImageView) view.findViewById(R.id.imageView_callcenter);


        listView_main_analyst = (ListView) view.findViewById(R.id.listView_main_analyst);
        mainAnalystListAdapter = new MainAnalystListAdapter(getActivity(), SharedPreferencesManager.getInstance().getListValue(SharedPreferencesManager.KEY_ANALYST_ALL));

        listView_main_analyst.setAdapter(mainAnalystListAdapter);
        imageView_callcenter.setOnClickListener(onClickListener);

        BannerAdapter adapter = new BannerAdapter(getActivity());
        autoscrollviewpager_select.addOnPageChangeListener(onPageChangeListener);
        autoscrollviewpager_select.setAdapter(adapter);
        autoscrollviewpager_select.setInterval(Constants.DEFAULT_VIEWPAGER_INTERVAL);
        autoscrollviewpager_select.startAutoScroll();
        navigationView = ((MainActivity) getActivity()).getNavigationView();
        toolbar = ((MainActivity) getActivity()).getToolbar();

        for (int i = 0; i < navigationView.getMenu().size(); i++) {
            navigationView.getMenu().getItem(i).setChecked(false);
        }

        Log.d(TAG, "initializationSelectBroadcastFragment()");
    }

    @Override
    public void onResume() {
        toolbar.setVisibility(View.VISIBLE);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        showDialog(getString(R.string.end_application_title), getString(R.string.end_application_message), getString(R.string.ok), getString(R.string.cancel));

                    }
                    return true;
                }
                return false;
            }
        });
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse(Constants.CALL_CENTER_NUMBER));
            startActivity(i);
        }
    };

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    indicator_00.setImageResource(R.drawable.indicator_select);
                    indicator_01.setImageResource(R.drawable.indicator_nonselect);
                    indicator_02.setImageResource(R.drawable.indicator_nonselect);
                    break;

                case 1:
                    indicator_00.setImageResource(R.drawable.indicator_nonselect);
                    indicator_01.setImageResource(R.drawable.indicator_select);
                    indicator_02.setImageResource(R.drawable.indicator_nonselect);
                    break;

                case 2:
                    indicator_00.setImageResource(R.drawable.indicator_nonselect);
                    indicator_01.setImageResource(R.drawable.indicator_nonselect);
                    indicator_02.setImageResource(R.drawable.indicator_select);
                    break;

            }
//            Log.d(TAG, "onPageChangeListener onPageSelected select indicator :: " + position + "...INTERVAL : " + DEFAULT_VIEWPAGER_INTERVAL);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void showDialog(String title, String message, String positiveButtonMessage, String negativeButtonMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(positiveButtonMessage, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.cancel();
                        getActivity().moveTaskToBack(true);
                        getActivity().finish();
                    }
                })
                .setNegativeButton(negativeButtonMessage, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
