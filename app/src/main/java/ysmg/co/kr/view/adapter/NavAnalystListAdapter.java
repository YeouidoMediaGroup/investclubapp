package ysmg.co.kr.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ysmg.co.kr.R;
import ysmg.co.kr.vo.analyst.AnalystBroadcast;

/**
 * Created by user on 2017-04-05.
 */

public class NavAnalystListAdapter extends BaseAdapter {
    static private String TAG = NavAnalystListAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<AnalystBroadcast> analystBroadcasts;
    private TextView textView_analyst_nick;
    public NavAnalystListAdapter(Context context, ArrayList<AnalystBroadcast> analystBroadcasts) {
        this.context = context;
        this.analystBroadcasts = analystBroadcasts;

    }

    @Override
    public int getCount() {

        return analystBroadcasts.size();
    }

    @Override
    public AnalystBroadcast getItem(int position) {

        return analystBroadcasts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.listview_item_nav_analyst, parent, false);
        textView_analyst_nick =  (TextView) view.findViewById(R.id.textView_analyst_nick);

        textView_analyst_nick.setText(analystBroadcasts.get(position).getNick());

        return view;
    }


}
