package ysmg.co.kr.view.fragment;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;
import ysmg.co.kr.R;
import ysmg.co.kr.network.SocketIOListener;
import ysmg.co.kr.network.SocketIOManager;
import ysmg.co.kr.util.Constants;
import ysmg.co.kr.util.LCDManager;
import ysmg.co.kr.util.MyFragmentManager;
import ysmg.co.kr.util.SharedPreferencesManager;
import ysmg.co.kr.view.activity.MainActivity;
import ysmg.co.kr.vo.reply.ReplyBanGroup;
import ysmg.co.kr.vo.reply.ReplyDisposeGroup;
import ysmg.co.kr.vo.reply.ReplyJoinGroup;
import ysmg.co.kr.vo.reply.ReplyReceiveMessage;
import ysmg.co.kr.vo.reply.ReplyReceiveNotice;
import ysmg.co.kr.vo.reply.ReplyRefreshUserlist;

public class BroadcastFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    static private final String TAG = BroadcastFragment.class.getSimpleName();
    static private final String SEPARATOR = "separator";
    static private final String DATA = "data";
    static private final int WRITE_TEXTVIEW_TIME = 100;
    static private final String REPLY_INIT = "";
    private String separator;
    private String data;
    private View view;
    private Toolbar toolbar;
    private LinearLayout linearLayout_release_Fullscreen;
    private FrameLayout frameLayout_videoView, frameLayout_fullscreen;
    private ScrollView scrollview_reply;
    private VideoView videoview_streaming;
    private ProgressBar progressbar_reply_loading, progressBar_Release_Fullscreen, progressBar_Fullscreen;
    private ImageButton button_reply_enter;
    private TextView textview_reply;
    private EditText edittext_reply;
    private SocketIOManager socketIOManager;

    private Timer testSendMessageTimer;
    private int i = 0;

    //
    public BroadcastFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param separator separator
     * @param data      data
     * @return A new instance of fragment BroadcastFragment.
     */
    public static BroadcastFragment newInstance(String separator, String data) {
        BroadcastFragment fragment = new BroadcastFragment();
        Bundle args = new Bundle();
        args.putString(SEPARATOR, separator);
        args.putString(DATA, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            separator = getArguments().getString(SEPARATOR);
            data = getArguments().getString(DATA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_broadcast, container, false);
        this.initializationBroadcastFragment(view);
        this.initializationBroadcast();
        this.initializationReply();

        return view;
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume() : ");
        scrollview_reply.fullScroll(ScrollView.FOCUS_DOWN);
        getView().setFocusableInTouchMode(true); // 키보
        getView().requestFocus();
        getView().setOnKeyListener(onKeyListener);
        LCDManager.getInstance().setLCDAlwaysOn();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy() : ");
        socketIOManager.disconnect();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        LCDManager.getInstance().setLCDOff();
//        getActivity().unregisterReceiver(earphoneReceiver);

        super.onDestroy();
    }

    private void initializationReply() {

        socketIOManager = SocketIOManager.createInstance();
        socketIOManager.setCallback(socketIOCallback);
        socketIOManager = SocketIOManager.getInstance(Constants.URL_REPLY_DEVELOP);
        progressbar_reply_loading.setVisibility(View.VISIBLE);
        edittext_reply.setOnKeyListener(onKeyListener);

        button_reply_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollview_reply.fullScroll(ScrollView.FOCUS_DOWN);
                socketIOManager.emitSendMessage(edittext_reply.getText().toString());
                edittext_reply.setText(null);
            }
        });

        Log.d(TAG, "initializationReply() : ");
    }

    private void initializationBroadcastFragment(View view) {

        frameLayout_videoView = (FrameLayout) view.findViewById(R.id.frameLayout_videoView);
        frameLayout_fullscreen = (FrameLayout) view.findViewById(R.id.frameLayout_fullscreen);
        linearLayout_release_Fullscreen = (LinearLayout) view.findViewById(R.id.linearLayout_release_Fullscreen);
        scrollview_reply = (ScrollView) view.findViewById(R.id.scrollview_reply);
        button_reply_enter = (ImageButton) view.findViewById(R.id.button_reply_enter);
        videoview_streaming = (VideoView) view.findViewById(R.id.videoview_streaming);
        progressbar_reply_loading = (ProgressBar) view.findViewById(R.id.progressbar_reply_loading);
        progressBar_Release_Fullscreen = (ProgressBar) view.findViewById(R.id.progressBar_Release_Fullscreen);
        progressBar_Fullscreen = (ProgressBar) view.findViewById(R.id.progressBar_Fullscreen);

        textview_reply = (TextView) view.findViewById(R.id.textview_reply);
        edittext_reply = (EditText) view.findViewById(R.id.edittext_reply);

        textview_reply.setText(this.REPLY_INIT);

        toolbar = ((MainActivity) getActivity()).getToolbar();
        toolbar.setVisibility(View.VISIBLE);

        Log.d(TAG, "initializationBroadcastFragment() : ");
    }

    private void initializationBroadcast() {

        videoview_streaming.setVideoPath(Constants.URL_STREAMMING_DEVELOP);
        videoview_streaming.setMediaController(new MediaController(getActivity()));
        videoview_streaming.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                frameLayout_fullscreen.setVisibility(View.INVISIBLE);
                linearLayout_release_Fullscreen.setVisibility(View.INVISIBLE);
                Log.d(TAG, "방송 로딩 완료");
                mp.start();
            }
        });

        videoview_streaming.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mp) {
                Log.d(TAG, "setOnSeekCompleteListener SEEK COMPLETE");
                initializationBroadcast();
            }
        });

        videoview_streaming.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.d(TAG, "setOnErrorListener 오류 발생");
                Toast.makeText(getActivity(), getString(R.string.broadcast_not_message), Toast.LENGTH_SHORT).show();
                showDialog(getString(R.string.broadcast_title), getString(R.string.broadcast_not_message), getString(R.string.ok));
                return false;
            }
        });

        videoview_streaming.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.d(TAG, "setOnCompletionListener Completion");
                if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    Log.d(TAG, "setOnCompletionListener ORIENTATION_LANDSCAPE");
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                } else {
                    Log.d(TAG, "setOnCompletionListener ORIENTATION_PORTRAIT");
                    frameLayout_fullscreen.setVisibility(View.INVISIBLE);
                    linearLayout_release_Fullscreen.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), getString(R.string.broadcast_end_message), Toast.LENGTH_SHORT).show();
                    showDialog(getString(R.string.broadcast_title), getString(R.string.broadcast_end_message), getString(R.string.ok));

                }
            }
        });

        Log.d(TAG, "initializationBroadcast() : ");
    }

    private View.OnKeyListener onKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {

            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_ENTER) {
                scrollview_reply.fullScroll(ScrollView.FOCUS_DOWN);
                socketIOManager.emitSendMessage(edittext_reply.getText().toString());
                edittext_reply.setText(null);
            } else if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    MyFragmentManager.getInstance().replaceFragment(R.id.fragment_container_main, getActivity(), SelectBroadcastFragment.newInstance());

                }
                return true;
            }
            return false;
        }
    };
    private SocketIOListener socketIOCallback = new SocketIOListener() {

        @Override
        public void socketConnectListener(boolean socketConnection) {
            Log.d(TAG, "SocketIOListener socketConnectListener : " + socketConnection);
        }

        @Override
        public void joingroupListener(final ReplyJoinGroup replyJoinGroup) {
            Log.d(TAG, "SocketIOListener joingroupListener : " + replyJoinGroup.toString());
            Handler replyLoadingHandler = new Handler(Looper.getMainLooper());
            replyLoadingHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (replyJoinGroup.isSuccess())
                        progressbar_reply_loading.setVisibility(View.INVISIBLE);
//                    else
//                        Toast.makeText(getContext(), replyJoinGroup.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        }

        @Override
        public void banGroupListener(ReplyBanGroup replyBanGroup) {
            Log.d(TAG, "SocketIOListener banGroupListener : " + replyBanGroup.toString());
        }

        @Override
        public void disposeGroupListener(ReplyDisposeGroup replyDisposeGroup) {
            Log.d(TAG, "SocketIOListener disposeGroupListener : " + replyDisposeGroup.toString());
        }

        @Override
        public void receiveMessageListener(ReplyReceiveMessage replyReceiveMessage) {
            final StringBuilder stringBuilder = new StringBuilder();
            int color = Color.BLACK;
            stringBuilder.append("\n").append("[").append(replyReceiveMessage.getTime()).append("] ")
                    .append(replyReceiveMessage.getNick()).append(" : ")
                    .append(replyReceiveMessage.getMessage());
            int startIndex = replyReceiveMessage.getTime().length() + 4; //\n, [, ], space
            int endIndex = startIndex + replyReceiveMessage.getNick().length();
            final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(stringBuilder.toString());
            if ((replyReceiveMessage.getNick().equals(SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_NICK)))) {
                color = Color.BLUE;
            }
            spannableStringBuilder.setSpan(new ForegroundColorSpan(color), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            Handler textviewHandler = new Handler(Looper.getMainLooper());
            textviewHandler.post(new Runnable() {
                @Override
                public void run() {
                    textview_reply.append(spannableStringBuilder);
                }
            });
            Handler scrollviewHandler = new Handler(Looper.getMainLooper());
            scrollviewHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    scrollview_reply.fullScroll(ScrollView.FOCUS_DOWN);
                }
            }, WRITE_TEXTVIEW_TIME);
            Log.d(TAG, "SocketIOListener receiveMessageListener : " + replyReceiveMessage.toString());
        }

        @Override
        public void receiveNoticeListener(ReplyReceiveNotice replyReceiveNotice) {
            StringBuilder stringBuilder = new StringBuilder();
            int color = Color.RED;
            stringBuilder.append("\n").append("[").append(replyReceiveNotice.getTime()).append("] ")
                    .append(replyReceiveNotice.getNick()).append(" : ")
                    .append(replyReceiveNotice.getMessage());
            int startIndex = replyReceiveNotice.getTime().length() + 4; //\n, [, ], space
            int endIndex = startIndex + replyReceiveNotice.getNick().length();
            final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(stringBuilder.toString());
            spannableStringBuilder.setSpan(new ForegroundColorSpan(color), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            Handler textviewHandler = new Handler(Looper.getMainLooper());
            textviewHandler.post(new Runnable() {
                @Override
                public void run() {
                    textview_reply.append(spannableStringBuilder);
                }
            });

            Handler scrollviewHandler = new Handler(Looper.getMainLooper());
            scrollviewHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    scrollview_reply.fullScroll(ScrollView.FOCUS_DOWN);
                }
            }, WRITE_TEXTVIEW_TIME);

            Log.d(TAG, "SocketIOListener receiveNoticeListener : " + replyReceiveNotice.toString());
        }

        @Override
        public void refreshUserlistListener(ReplyRefreshUserlist replyRefreshUserlist) {
            Log.d(TAG, "SocketIOListener refreshUserlistListener : " + replyRefreshUserlist.toString());
        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "onConfigurationChanged() : " + newConfig.orientation);

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            toolbar.setVisibility(View.VISIBLE);
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 280, getResources().getDisplayMetrics())
            );
            frameLayout_videoView.setLayoutParams(layoutParams);
            frameLayout_fullscreen.setVisibility(View.INVISIBLE);
            linearLayout_release_Fullscreen.setVisibility(View.VISIBLE);
            initializationBroadcast();


        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {//세로->가로
            toolbar.setVisibility(View.GONE);
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(
                    ConstraintLayout.LayoutParams.MATCH_PARENT,
                    ConstraintLayout.LayoutParams.MATCH_PARENT
            );
            frameLayout_videoView.setLayoutParams(layoutParams);
            frameLayout_fullscreen.setVisibility(View.VISIBLE);
            linearLayout_release_Fullscreen.setVisibility(View.INVISIBLE);
            initializationBroadcast();
        }
    }

    private void showDialog(String title, String message, String buttonMessage) {
        LCDManager.getInstance().setLCDOff();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(buttonMessage, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        progressBar_Release_Fullscreen.setVisibility(View.INVISIBLE);
                        progressBar_Fullscreen.setVisibility(View.INVISIBLE);
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    //TODO: delete testcode
    public void resetLimitCallTimer() {
        if (testSendMessageTimer == null) {
            testSendMessageTimer = new Timer();
            testSendMessageTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    socketIOManager.emitSendMessage("루프 테스트 중입니다." + i++);
                }
            }, 1000, 1000);
        }
    }
}
