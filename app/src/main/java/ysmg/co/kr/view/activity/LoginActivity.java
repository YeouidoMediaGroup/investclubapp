package ysmg.co.kr.view.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.Gson;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import ysmg.co.kr.R;
import ysmg.co.kr.network.HttpManager;
import ysmg.co.kr.network.SocketIOManager;
import ysmg.co.kr.util.Constants;
import ysmg.co.kr.util.LCDManager;
import ysmg.co.kr.util.MyFragmentManager;
import ysmg.co.kr.util.MySettingInfomation;
import ysmg.co.kr.util.SharedPreferencesManager;
import ysmg.co.kr.vo.LoginInfomation;
import ysmg.co.kr.vo.analyst.AnalystAll;
import ysmg.co.kr.vo.analyst.AnalystAllInfomation;
import ysmg.co.kr.vo.analyst.AnalystBroadcast;

public class LoginActivity extends AppCompatActivity {
    static private String TAG = LoginActivity.class.getSimpleName();
    private ImageButton button_login, button_register;
    private EditText editText_id, editText_pw;
    private CheckBox checkBox_autoLogin;
    private StringBuffer stringBuffer;
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.requestPermission();
        this.initializationLoginActivity();
        this.autoLogin();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initializationLoginActivity() {

        SocketIOManager.createInstance();
        SharedPreferencesManager.createInstance(this);
        MyFragmentManager.createInstance(this);
        MySettingInfomation.createInstance(this);
        LCDManager.createInstance(this).setWindow(getWindow());

        gson = new Gson();
        button_login = (ImageButton) findViewById(R.id.button_login);
        button_register = (ImageButton) findViewById(R.id.button_register);

        editText_id = (EditText) findViewById(R.id.editText_id);
        editText_pw = (EditText) findViewById(R.id.editText_pw);

        checkBox_autoLogin = (CheckBox) findViewById(R.id.checkBox_autoLogin);

        button_login.setOnClickListener(onClickListener);
        button_register.setOnClickListener(onClickListener);

        editText_id.setText(SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_ID));
        editText_pw.setText(SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_PW));

        Log.d(TAG, "LoginActivity initializationLoginActivity()  ");
    }

    private void autoLogin() {
        if (checkBox_autoLogin.isChecked() && editText_pw.getText().length() != 0) {
            startLogin();

        }
    }

    /**
     * when users login or logout, This method works validating data and displaying dialog box.
     */
    private void startLogin() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        HttpManager.GETPOSTRequestThread task = new HttpManager.GETPOSTRequestThread();

        stringBuffer = new StringBuffer();
        stringBuffer
                .append(Constants.ID).append(editText_id.getText().toString())
                .append(Constants.AMPERSAND)
                .append(Constants.PW).append(editText_pw.getText().toString());

        String body = stringBuffer.toString();
        try {
            String result = task.execute(Constants.POST, Constants.URL_LOGIN, body).get();
            LoginInfomation loginInfomation = (LoginInfomation) gson.fromJson(result, LoginInfomation.class);
            loginInfomation.setId(editText_id.getText().toString());
            loginInfomation.setPw(editText_pw.getText().toString());

//            Log.d(TAG, "startLogin LoginInfomation info : " + loginInfomation.toString());
            if (loginInfomation.isSuccess()) {
                ArrayList<AnalystBroadcast> analystBroadcasts = new ArrayList<AnalystBroadcast>(Arrays.asList(loginInfomation.getAnalyst()));
                SharedPreferencesManager.getInstance().setListValue(SharedPreferencesManager.KEY_ANALYST_BROADCAST, analystBroadcasts);
                getgGHm();

                stringBuffer = new StringBuffer();
                stringBuffer.append(getString(R.string.login_success_head)).
                        append(loginInfomation.getNick()).
                        append(getString(R.string.login_success_tail));

                showDialog(getString(R.string.login_title), stringBuffer.toString(), getString(R.string.ok));
                LoginInfomation.updateLoginInfomation(loginInfomation);
            } else {
                editText_pw.setError(getString(R.string.login_fail));
                editText_pw.requestFocus();
                LoginInfomation.updateLoginInfomation(null);

            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void getgGHm() throws ExecutionException, InterruptedException {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("h H HH MM");
        DateTime dateTime = DateTime.now();

        HttpManager.GETPOSTRequestThread task1 = new HttpManager.GETPOSTRequestThread();
        stringBuffer = new StringBuffer();

        String body = stringBuffer.append(Constants.MODE).append(dateTime.toString(formatter).replaceAll(" ", "")).toString();
        String result = task1.execute(Constants.GET, Constants.URL_ANALYST, body).get();
        Log.d(TAG, "AnalystAllInfomation result : " + result);

        AnalystAllInfomation allAnalystInfomation = (AnalystAllInfomation) gson.fromJson(result, AnalystAllInfomation.class);
        ArrayList<AnalystAll> analystAlls = new ArrayList<AnalystAll>(Arrays.asList(allAnalystInfomation.getAnalyst()));
        SharedPreferencesManager.getInstance().setListValue(SharedPreferencesManager.KEY_ANALYST_ALL, analystAlls);
//        Log.d(TAG, "AnalystAllInfomation allAnalystInfomation : " + allAnalystInfomation.toString());

    }

    private void showDialog(String title, String message, String buttonMessage) {
        LCDManager.getInstance().setLCDOff();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(buttonMessage, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.putExtra(Constants.INTENT_GETPUSH, getIntent().getStringExtra(Constants.INTENT_GETPUSH));
                        Log.d(TAG, "showDialog getIntent().getStringExtra(Constants.INTENT_GETPUSH) : " + getIntent().getStringExtra(Constants.INTENT_GETPUSH));

                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_login:
                    startLogin();
                    break;

                case R.id.button_register:
                    startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
//                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.상한가투자클럽.com"));
//                    startActivity(browserIntent);
                    break;
            }
        }
    };
    private boolean requestPermission() {
        int internet = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
        int call_phone = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        int read_phone_state = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int read_sms = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);

        List<String> listPermissions = new ArrayList<>();

        if (internet != PackageManager.PERMISSION_GRANTED) {
            listPermissions.add(android.Manifest.permission.INTERNET);
        }

        if (call_phone != PackageManager.PERMISSION_GRANTED) {
            listPermissions.add(android.Manifest.permission.CALL_PHONE);
        }

        if (read_phone_state != PackageManager.PERMISSION_GRANTED) {
            listPermissions.add(android.Manifest.permission.READ_PHONE_STATE);
        }

        if (read_sms != PackageManager.PERMISSION_GRANTED) {
            listPermissions.add(android.Manifest.permission.READ_SMS);
        }

        Log.d(TAG, "AnalystAllInfomation requestPermission : " + listPermissions.toString());

        if (!listPermissions.isEmpty())
        {
            ActivityCompat.requestPermissions(this,listPermissions.toArray
                    (new String[listPermissions.size()]), Constants.PERMISSIONS_REQUEST_CODE);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.PERMISSIONS_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0) {
                    for(int result : grantResults) {
                        if (result == PackageManager.PERMISSION_DENIED) {
                            Toast.makeText(getApplicationContext(), "권한허용을 하지않으면 앱을 실행할 수 없습니다.", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
