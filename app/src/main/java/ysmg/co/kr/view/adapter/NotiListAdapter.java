package ysmg.co.kr.view.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import ysmg.co.kr.R;
import ysmg.co.kr.network.HttpManager;
import ysmg.co.kr.util.Constants;
import ysmg.co.kr.util.SharedPreferencesManager;
import ysmg.co.kr.vo.Notification;
import ysmg.co.kr.vo.Success;

/**
 * Created by user on 2017-04-05.
 */

public class NotiListAdapter extends BaseAdapter {
    static private String TAG = NotiListAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<Notification> notifications;
    private TextView textView_noti_contents, textView_Time, textView_noti_title, textView_noti_contents_summary, textView_expander;
    private ImageButton button_UpDown;
    private ConstraintLayout constraintLayout_noti_mains;
    private Gson gson;
    public NotiListAdapter(Context context, ArrayList<Notification> notifications) {
        this.context = context;
        this.notifications = notifications;

    }

    @Override
    public int getCount() {

        return notifications.size() + 1;
    }

    @Override
    public Notification getItem(int position) {

        return notifications.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.listview_item_noti, parent, false);
        gson = new Gson();
        textView_noti_title =  (TextView) view.findViewById(R.id.textView_noti_title);
        textView_noti_contents_summary =  (TextView) view.findViewById(R.id.textView_noti_contents_summary);
        textView_noti_contents =  (TextView) view.findViewById(R.id.textView_noti_contents);
        textView_Time =  (TextView) view.findViewById(R.id.textView_Time);
        button_UpDown =  (ImageButton) view.findViewById(R.id.button_UpDown);
        constraintLayout_noti_mains =  (ConstraintLayout) view.findViewById(R.id.constraintLayout_noti_mains);
        textView_expander = (TextView) view.findViewById(R.id.textView_expander);

        if(position == (notifications.size())){
            textView_noti_title.setVisibility(View.GONE);
            textView_noti_contents_summary.setVisibility(View.GONE);
            textView_noti_contents.setVisibility(View.GONE);
            textView_Time.setVisibility(View.GONE);
            button_UpDown.setVisibility(View.GONE);
            textView_expander.setVisibility(View.VISIBLE);
            return view;
        }

        String contents_summary = notifications.get(position).getContents();
        if(contents_summary.length() > 20)
            contents_summary = contents_summary.substring(0, 20);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(contents_summary).append("...");

        textView_noti_title.setText(notifications.get(position).getTitle());
        textView_noti_contents_summary.setText(stringBuilder.toString());
        textView_noti_contents.setText(notifications.get(position).getContents());
        textView_Time.setText(notifications.get(position).getTime());
        textView_noti_contents.setVisibility(View.GONE);

        if(notifications.get(position).isread()){
            textView_noti_title.setTextColor(ContextCompat.getColor(context, R.color.colorGray));
            textView_noti_contents_summary.setTextColor(ContextCompat.getColor(context, R.color.colorGray));
            textView_Time.setTextColor(ContextCompat.getColor(context, R.color.colorGray));

        } else {
            textView_noti_title.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));
            textView_noti_contents_summary.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));
            textView_Time.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));

        }

        constraintLayout_noti_mains.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView_click_noti_title = (TextView) v.findViewById(R.id.textView_noti_title);
                TextView textView_click_noti_contents = (TextView) v.findViewById(R.id.textView_noti_contents);
                TextView textView_click_noti_conntents_summary = (TextView) v.findViewById(R.id.textView_noti_contents_summary);
                TextView textView_click_Time = (TextView) v.findViewById(R.id.textView_Time);

                ImageButton button_UpDown = (ImageButton) v.findViewById(R.id.button_UpDown);

                if (textView_click_noti_contents.getVisibility() == View.VISIBLE) {
                    textView_click_noti_contents.setVisibility(View.GONE);
                    textView_click_noti_conntents_summary.setVisibility(View.VISIBLE);
                    button_UpDown.setBackgroundResource(R.drawable.ic_keyboard_arrow_down_black);
                }

                else if (textView_click_noti_contents.getVisibility() == View.GONE) {
                    textView_click_noti_contents.setVisibility(View.VISIBLE);
                    textView_click_noti_conntents_summary.setVisibility(View.GONE);
                    button_UpDown.setBackgroundResource(R.drawable.ic_keyboard_arrow_up_black);
                    HttpManager.GETPOSTRequestThread task = new HttpManager.GETPOSTRequestThread();
                    try {
                        String pushresult = task.execute(Constants.POST, Constants.URL_PUSH, getPushbody(notifications.get(position).getPush_id())).get();
                        Log.d(TAG, "pushresult : " + pushresult);
                        Success success = (Success) gson.fromJson(pushresult, Success.class);
                        if(success.isSuccess()) {
                            notifications.get(position).setIsread(true);
                            textView_click_noti_title.setTextColor(ContextCompat.getColor(context, R.color.colorGray));
                            textView_click_noti_conntents_summary.setTextColor(ContextCompat.getColor(context, R.color.colorGray));
                            textView_click_Time.setTextColor(ContextCompat.getColor(context, R.color.colorGray));
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        return view;
    }

    private String getPushbody(String push_id) {
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer
                .append(Constants.ID).append(SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_ID)).append(Constants.AMPERSAND)
                .append(Constants.MODE).append(Constants.MODE_READ_PUSH).append(Constants.AMPERSAND)
                .append(Constants.PUSH_ID).append(push_id).append(Constants.AMPERSAND)
                .append(Constants.PAGE).append("");

        return stringBuffer.toString();
    }
}
