package ysmg.co.kr.vo.analyst;

/**
 * Created by user on 2017-03-07.
 */

public class AnalystBroadcast {
    private int anal_no;//애널리스트 번호
    private String nick;//애널리스트 별명
    private int room_no;//애널리스트 방번호
    private String live_key;//애널리스트 방이름

    public AnalystBroadcast(int anal_no, String nick, int room_no, String live_key) {
        this.anal_no = anal_no;
        this.nick = nick;
        this.room_no = room_no;
        this.live_key = live_key;
    }

    public int getAnal_no() {
        return anal_no;
    }

    public void setAnal_no(int anal_no) {
        this.anal_no = anal_no;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getRoom_no() {
        return room_no;
    }

    public void setRoom_no(int room_no) {
        this.room_no = room_no;
    }

    public String getLive_key() {
        return live_key;
    }

    public void setLive_key(String live_key) {
        this.live_key = live_key;
    }

    @Override
    public String toString() {
        return "AnalystBroadcast{" +
                "anal_no=" + anal_no +
                ", nick='" + nick + '\'' +
                ", room_no=" + room_no +
                ", live_key='" + live_key + '\'' +
                '}';
    }
}
