package ysmg.co.kr.vo;

/**
 * Created by user on 2017-03-09.
 */

public class SignUpResponse {
    private boolean success;
    private String message;

    public SignUpResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "SignUpResponse{" +
                "success=" + success +
                ", message='" + message + '\'' +
                '}';
    }
}
