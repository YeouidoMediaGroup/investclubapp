package ysmg.co.kr.vo;

/**
 * Created by user on 2017-04-14.
 */

public class Notification {
    private String push_id;
    private String title;
    private String contents;
    private String time;
    private boolean isread;

    public Notification(String push_id, String title, String contents, String time, boolean isread) {
        this.push_id = push_id;
        this.title = title;
        this.contents = contents;
        this.time = time;
        this.isread = isread;
    }

    public String getPush_id() {
        return push_id;
    }

    public void setPush_id(String push_id) {
        this.push_id = push_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isread() {
        return isread;
    }

    public void setIsread(boolean isread) {
        this.isread = isread;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "push_id='" + push_id + '\'' +
                ", title='" + title + '\'' +
                ", contents='" + contents + '\'' +
                ", time='" + time + '\'' +
                ", isread=" + isread +
                '}';
    }
}

