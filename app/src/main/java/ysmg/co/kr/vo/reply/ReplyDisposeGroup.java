package ysmg.co.kr.vo.reply;

/**
 * Created by user on 2017-03-14.
 */

public class ReplyDisposeGroup {
    private String message;

    public ReplyDisposeGroup(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ReplyDisposeGroup{" +
                "message='" + message + '\'' +
                '}';
    }
}
