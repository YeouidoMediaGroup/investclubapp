package ysmg.co.kr.vo.reply;

import java.util.Arrays;

/**
 * Created by user on 2017-03-14.
 */

public class ReplyReceiveMessage {
    private String time; //HH:mm
    private String nick;
    private String message;
    private String[] data;

    public ReplyReceiveMessage(String time, String nick, String message, String[] data) {
        this.time = time;
        this.nick = nick;
        this.message = message;
        this.data = data;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String[] getData() {
        return data;
    }

    public void setData(String[] data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ReplyReceiveMessage{" +
                "time=" + time +
                ", nick='" + nick + '\'' +
                ", message='" + message + '\'' +
                ", data=" + Arrays.toString(data) +
                '}';
    }
}
