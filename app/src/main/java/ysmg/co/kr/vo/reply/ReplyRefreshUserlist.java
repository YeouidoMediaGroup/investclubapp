package ysmg.co.kr.vo.reply;

import java.util.Arrays;

/**
 * Created by user on 2017-03-14.
 */

public class ReplyRefreshUserlist {
    private String owner;
    private String[] user;

    public ReplyRefreshUserlist(String owner, String[] user) {
        this.owner = owner;
        this.user = user;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String[] getUser() {
        return user;
    }

    public void setUser(String[] user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "ReplyRefreshUserlist{" +
                "owner='" + owner + '\'' +
                ", user=" + Arrays.toString(user) +
                '}';
    }
}
