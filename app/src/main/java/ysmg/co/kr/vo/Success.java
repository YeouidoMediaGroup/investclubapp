package ysmg.co.kr.vo;

/**
 * Created by user on 2017-04-13.
 */

public class Success {
    private boolean success;

    public Success(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "Success{" +
                "success=" + success +
                '}';
    }
}
