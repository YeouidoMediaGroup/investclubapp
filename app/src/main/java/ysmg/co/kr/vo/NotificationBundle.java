package ysmg.co.kr.vo;

import java.util.Arrays;

/**
 * Created by user on 2017-04-14.
 */

public class NotificationBundle {
    private int length;
    private Notification[] push;
    

    public NotificationBundle(Notification[] push, int length) {
        this.push = push;
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public Notification[] getPush() {
        return push;
    }

    public void setPush(Notification[] push) {
        this.push = push;
    }

    @Override
    public String toString() {
        return "NotificationBundle{" +
                "length=" + length +
                ", push=" + Arrays.toString(push) +
                '}';
    }
}
