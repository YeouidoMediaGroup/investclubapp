package ysmg.co.kr.vo.reply;

/**
 * Created by user on 2017-03-13.
 */

public class ReplyJoinGroup {
    private boolean success;
    private String message;

    public ReplyJoinGroup(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ReplyJoinGroup{" +
                "success=" + success +
                ", message='" + message + '\'' +
                '}';
    }
}
