package ysmg.co.kr.vo.reply;

/**
 * Created by user on 2017-03-14.
 */

public class ReplyBanGroup {
    private String message;

    public ReplyBanGroup(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ReplyBanGroup{" +
                "message='" + message + '\'' +
                '}';
    }
}
