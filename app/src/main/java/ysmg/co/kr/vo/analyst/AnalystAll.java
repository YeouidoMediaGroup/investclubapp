package ysmg.co.kr.vo.analyst;

/**
 * Created by user on 2017-03-07.
 */

public class AnalystAll {
    private int anal_no; //애널리스트 번호
    private String nick; //애널리스트 별명
    private int room_no; //애널리스트 방번호
    private String live_key; //애널리스트 방이름
    private String content; //애널리스트 소개
    private String profile; //애널리스트 프로필

    public AnalystAll(int anal_no, String nick, int room_no, String live_key, String content, String profile) {
        this.anal_no = anal_no;
        this.nick = nick;
        this.room_no = room_no;
        this.live_key = live_key;
        this.content = content;
        this.profile = profile;
    }

    public int getAnal_no() {
        return anal_no;
    }

    public void setAnal_no(int anal_no) {
        this.anal_no = anal_no;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getRoom_no() {
        return room_no;
    }

    public void setRoom_no(int room_no) {
        this.room_no = room_no;
    }

    public String getLive_key() {
        return live_key;
    }

    public void setLive_key(String live_key) {
        this.live_key = live_key;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    @Override
    public String toString() {
        return "AnalystAll{" +
                "anal_no=" + anal_no +
                ", nick='" + nick + '\'' +
                ", room_no=" + room_no +
                ", live_key='" + live_key + '\'' +
                ", content='" + content + '\'' +
                ", profile='" + profile + '\'' +
                '}'
                + "\n\n";
    }
}
