package ysmg.co.kr.vo.analyst;

import java.util.Arrays;

/**
 * Created by user on 2017-03-07.
 */

public class AnalystAllInfomation {
    private boolean success;
    private AnalystAll[] analyst;

    public AnalystAllInfomation(boolean success, AnalystAll[] analyst) {
        this.success = success;
        this.analyst = analyst;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public AnalystAll[] getAnalyst() {
        return analyst;
    }

    public void setAnalyst(AnalystAll[] analyst) {
        this.analyst = analyst;
    }

    @Override
    public String toString() {
        return "AnalystAllInfomation{" +
                "success=" + success +
                ", analyst=" + Arrays.toString(analyst) +
                '}';
    }
}
