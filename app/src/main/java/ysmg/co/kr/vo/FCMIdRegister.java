package ysmg.co.kr.vo;

/**
 * Created by user on 2017-04-17.
 */

@Deprecated
public class FCMIdRegister {
    private String	id;
    private String	pw;
    private String	fcm_id;
    private String	dev_id;
    private String	app_version;
    private String	device_os;
    private String	device_info;

    public FCMIdRegister(String id, String pw, String fcm_id, String dev_id, String app_version, String device_os, String device_info) {
        this.id = id;
        this.pw = pw;
        this.fcm_id = fcm_id;
        this.dev_id = dev_id;
        this.app_version = app_version;
        this.device_os = device_os;
        this.device_info = device_info;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public String getFcm_id() {
        return fcm_id;
    }

    public void setFcm_id(String fcm_id) {
        this.fcm_id = fcm_id;
    }

    public String getDev_id() {
        return dev_id;
    }

    public void setDev_id(String dev_id) {
        this.dev_id = dev_id;
    }

    public String getApp_version() {
        return app_version;
    }

    public void setApp_version(String app_version) {
        this.app_version = app_version;
    }

    public String getDevice_os() {
        return device_os;
    }

    public void setDevice_os(String device_os) {
        this.device_os = device_os;
    }

    public String getDevice_info() {
        return device_info;
    }

    public void setDevice_info(String device_info) {
        this.device_info = device_info;
    }

    @Override
    public String toString() {
        return "FCMIdRegister{" +
                "id='" + id + '\'' +
                ", pw='" + pw + '\'' +
                ", fcm_id='" + fcm_id + '\'' +
                ", dev_id='" + dev_id + '\'' +
                ", app_version='" + app_version + '\'' +
                ", device_os='" + device_os + '\'' +
                ", device_info='" + device_info + '\'' +
                '}';
    }
}
