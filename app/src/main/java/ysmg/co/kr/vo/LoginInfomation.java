package ysmg.co.kr.vo;

import android.util.Log;

import java.util.Arrays;

import ysmg.co.kr.util.SharedPreferencesManager;
import ysmg.co.kr.vo.analyst.AnalystBroadcast;

/**
 * Created by user on 2017-03-07.
 */

public class LoginInfomation {
    static private String TAG = LoginInfomation.class.getSimpleName();
    private boolean success;
    private String id;
    private String pw;
    private String message;
    private AnalystBroadcast[] analyst;
    private String nick;
    private int level;

    public LoginInfomation(boolean success, String id, String pw, String message, AnalystBroadcast[] analyst, String nick, int level) {
        this.success = success;
        this.id = id;
        this.pw = pw;
        this.message = message;
        this.analyst = analyst;
        this.nick = nick;
        this.level = level;
    }

    public static String getTAG() {
        return TAG;
    }

    public static void setTAG(String TAG) {
        LoginInfomation.TAG = TAG;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AnalystBroadcast[] getAnalyst() {
        return analyst;
    }

    public void setAnalyst(AnalystBroadcast[] analyst) {
        this.analyst = analyst;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "LoginInfomation{" +
                "success=" + success +
                ", id='" + id + '\'' +
                ", pw='" + pw + '\'' +
                ", message='" + message + '\'' +
                ", analyst=" + Arrays.toString(analyst) +
                ", nick='" + nick + '\'' +
                ", level=" + level +
                '}';
    }

    /**
     * This method developed for login session management.
     *
     * @param loginInfomation login infomation like id, pw, etc...
     */
    public static void updateLoginInfomation(LoginInfomation loginInfomation) {
        if (loginInfomation == null) {
            SharedPreferencesManager.getInstance().setBooleanValue(SharedPreferencesManager.KEY_SUCCESS, SharedPreferencesManager.DEFAULT_BOOLEAN);
            SharedPreferencesManager.getInstance().setStringValue(SharedPreferencesManager.KEY_PW, SharedPreferencesManager.DEFAULT_STRING);
            SharedPreferencesManager.getInstance().setStringValue(SharedPreferencesManager.KEY_MESSAGE, SharedPreferencesManager.DEFAULT_STRING);
            SharedPreferencesManager.getInstance().setStringSetValue(SharedPreferencesManager.KEY_ANALYST, SharedPreferencesManager.DEFAULT_SET_STRING);
            SharedPreferencesManager.getInstance().setStringValue(SharedPreferencesManager.KEY_NICK, SharedPreferencesManager.DEFAULT_STRING);
            SharedPreferencesManager.getInstance().setIntValue(SharedPreferencesManager.KEY_LEVEL, SharedPreferencesManager.DEFAULT_INT);

        } else {
            SharedPreferencesManager.getInstance().setBooleanValue(SharedPreferencesManager.KEY_SUCCESS, loginInfomation.isSuccess());
            SharedPreferencesManager.getInstance().setStringValue(SharedPreferencesManager.KEY_ID, loginInfomation.getId());
            SharedPreferencesManager.getInstance().setStringValue(SharedPreferencesManager.KEY_PW, loginInfomation.getPw());
            SharedPreferencesManager.getInstance().setStringValue(SharedPreferencesManager.KEY_MESSAGE, loginInfomation.getMessage());
//            SharedPreferencesManager.getInstance().setStringSetValue(SharedPreferencesManager.KEY_ANALYST, new HashSet<String>(Arrays.asList(loginInfomation.getAnalyst())));
            SharedPreferencesManager.getInstance().setStringValue(SharedPreferencesManager.KEY_NICK, loginInfomation.getNick());
            SharedPreferencesManager.getInstance().setIntValue(SharedPreferencesManager.KEY_LEVEL, loginInfomation.getLevel());
        }
        Log.d(TAG, "updateLoginInfomation SharedPreferencesManager " + "\n" +
                "KEY_SUCCESS : " + SharedPreferencesManager.getInstance().getBooleanValue(SharedPreferencesManager.KEY_SUCCESS) + "\n" +
                "KEY_ID : " + SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_ID) + "\n" +
                "KEY_MESSAGE : " + SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_MESSAGE) + "\n" +
                "KEY_ANALYST : " + SharedPreferencesManager.getInstance().getStringSetValue(SharedPreferencesManager.KEY_ANALYST) + "\n" +
                "KEY_NICK : " + SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_NICK) + "\n" +
                "KEY_LEVEL : " + SharedPreferencesManager.getInstance().getIntValue(SharedPreferencesManager.KEY_LEVEL));
    }
}
