package ysmg.co.kr.vo;

/**
 * Created by user on 2017-03-09.
 */

@Deprecated
public class SignUpRequest {
    private String mode;
    private int phone_num;
    private String id;
    private String pw;
    private String name;
    private String ip;
    private String nick;

    public SignUpRequest(String mode, int phone_num, String id, String pw, String name, String ip, String nick) {
        this.mode = mode;
        this.phone_num = phone_num;
        this.id = id;
        this.pw = pw;
        this.name = name;
        this.ip = ip;
        this.nick = nick;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public int getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(int phone_num) {
        this.phone_num = phone_num;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    @Override
    public String toString() {
        return "SignUpRequest{" +
                "mode='" + mode + '\'' +
                ", phone_num=" + phone_num +
                ", id='" + id + '\'' +
                ", pw='" + pw + '\'' +
                ", name='" + name + '\'' +
                ", ip='" + ip + '\'' +
                ", nick='" + nick + '\'' +
                '}';
    }
}
