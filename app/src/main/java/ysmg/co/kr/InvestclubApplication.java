package ysmg.co.kr;

import android.app.Application;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

/**
 * Created by user on 2017-03-24.
 */

public class InvestclubApplication extends Application{
    static private String TAG = InvestclubApplication.class.getSimpleName();
    @Override
    public void onCreate() {
        Log.d(TAG, "start Application");
        super.onCreate();

        Thread.setDefaultUncaughtExceptionHandler(uncaughtExceptionHandler);
    }

    private Thread.UncaughtExceptionHandler uncaughtExceptionHandler = new Thread.UncaughtExceptionHandler(){

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            Log.d(TAG, "fatal error catch uncaughtException !!!!!!!!");
            e.printStackTrace();
            FirebaseCrash.report(e);
        }
    };
}
