package ysmg.co.kr.firebase;

import android.os.Build;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;

import ysmg.co.kr.BuildConfig;
import ysmg.co.kr.network.HttpManager;
import ysmg.co.kr.util.Constants;
import ysmg.co.kr.util.MySettingInfomation;
import ysmg.co.kr.util.SharedPreferencesManager;
import ysmg.co.kr.vo.Success;


/**
 * Created by user on 2017-03-27.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    static private String TAG = MyFirebaseInstanceIDService.class.getSimpleName();
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
// [START refresh_token]
    @Override
    public void onTokenRefresh() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken);
    }
    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        Log.d(TAG, "sendRegistrationToServer token : " + token);
        HttpManager.GETPOSTRequestThread task = new HttpManager.GETPOSTRequestThread();
        Gson gson = new Gson();

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer
                .append(Constants.ID).append(SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_ID)).append(Constants.AMPERSAND)
                .append(Constants.PW).append(SharedPreferencesManager.getInstance().getStringValue(SharedPreferencesManager.KEY_PW)).append(Constants.AMPERSAND)
                .append(Constants.FCM_ID).append(token).append(Constants.AMPERSAND)
                .append(Constants.DEV_ID).append(MySettingInfomation.getInstance().getIMEI()).append(Constants.AMPERSAND)
                .append(Constants.APP_VERSION).append(BuildConfig.VERSION_NAME).append(Constants.AMPERSAND)
                .append(Constants.DEVICE_OS).append(Build.VERSION.RELEASE).append(Constants.AMPERSAND)
                .append(Constants.DEVICE_INFO).append(Build.MODEL);

        try {
            String result = task.execute(Constants.POST, Constants.URL_FCM_ID_REGISTER, stringBuffer.toString()).get();
            Success success = (Success) gson.fromJson(result, Success.class);

            if(!success.isSuccess())
                throw new Exception("fatal fcm regist fail : ");

        } catch (Exception e) {
            FirebaseCrash.report(e);

        }


    }
}
